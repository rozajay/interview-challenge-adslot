# Adslot Front End Test

The objective is to create a table through consuming APIs

## How the project progressed

Wow, that was a super fun front end task. Initiall upon receiving the task I spent sometime trying to understand how the APIs related to each other and what the best way is for the components to be rendered. I decided to first settle on a format with all the relevant information for front end then proceeding with configuring the APIs to form the datatype I needed for front end rendering. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development purposes.

### Installing

After unzipping the files

Install node modules
```
cd activity-feed
npm install
```
then
```
npm start
```

If the window doesn't automatically pop up, open the url 'http://localhost:3000/' to access the application locally

## Incomplete Areas
- Only basic searching functionaliti is acheived
- implementing debounce on keypress


## Areas of improvement
- Initial rendering of page is a bit laggy, can be improved using node service to stream
- Code is not production ready
- Ran out of time to write tests
- Need to explore better/ more efficient ways of consuming the APIs
- Tighter typing to be done overall
- More comments in code

## Author

* **Roza Jiang** 

