import { ApplicationState } from '../state/ApplicationState';

export const InitialState: ApplicationState = {
  tableEntries: [
    {
      bookings: [],
      sellerID: '12345',
      sellerName: 'Purdy, Rau and Walsh',
    }
  ],

  filteredTableEntries: [
    {
      bookings: [],
      sellerID: '12345',
      sellerName: 'Purdy, Rau and Walsh',
    }
  ]
}