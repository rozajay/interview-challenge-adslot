
import { ISingleEntry } from '../types/index'

export interface ApplicationState {
  tableEntries: ISingleEntry[],
  filteredTableEntries: ISingleEntry[]
}