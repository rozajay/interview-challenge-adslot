import * as moment from 'moment'
import * as constants from '../constants/DefaultConstants'

export interface ISellerInfo {
  sellerID: string,
  sellerName: string
}

export interface IUpdateSellersInfo {
  type: constants.UPDATE_SELLERS_INFO,
  payload: ISellerInfo[]
}

export interface IUpdateProductsInfo {
  type: constants.UPDATE_PRODUCTS_INFO,
  sellerID: string,
  productsInfo: any
}

export interface IUpdateBookingsInfo {
  type: constants.UPDATE_BOOKINGS_INFO,
  sellerID: string,
  bookingsInfo: any
}

export interface IInitiateFilteredTable {
  type: constants.INITIATE_FILTERED_TABLE_ENTRIES
}

export interface IUpdateFilteredTable {
  text: string,
  type: constants.UPDATE_FILTERED_TABLE_ENTRIES
}


export type ApplicationAction = IUpdateSellersInfo | IUpdateProductsInfo | IUpdateBookingsInfo | IInitiateFilteredTable | IUpdateFilteredTable

// Updating the view for search
export function updateFilteredTable(text: string): any {
  return {
    text,
    type: constants.UPDATE_FILTERED_TABLE_ENTRIES,
  }
}

// Fetch API Info
export function fetchTableInfo(): any {
  return (dispatch: any) => {

    const completeSellerInfo = fetch("https://blooming-fortress-38880.herokuapp.com/sellers")
      .then(response => response.json())
      .then(json => {
        const sellerInfo = json.data.map((data: any) => {
          return {
            sellerID: data.id,
            sellerName: data.name
          }
        })
        dispatch(updateSellerInfo(sellerInfo))
        return sellerInfo
      })

    completeSellerInfo.then(sellerInfo => {
      sellerInfo.map((singleSeller: any) => {
        fetch("https://blooming-fortress-38880.herokuapp.com/products")
          .then(response => response.json())
          .then(json => {
            const relevantProducts = json.data.filter((item: any) => item.sellerId === singleSeller.sellerID)
              .map((item: any) => {
                return {
                  clientName: '',
                  productID: item.id,
                  productName: item.name,
                  rate: item.rate,
                }
              })
            dispatch(updateProductsInfo(singleSeller.sellerID, relevantProducts))
            if (relevantProducts.length > 0) {
              relevantProducts.map((singleProduct: any) => {
                fetch("https://blooming-fortress-38880.herokuapp.com/bookings")
                  .then(response => response.json())
                  .then(productJson => {
                    const relevantBookings = productJson.data.filter((item: any) => item.productId === singleProduct.productID)
                      .map((item: any) => {
                        const currentDateTime = moment().format().toString()
                        return {
                          active: moment(currentDateTime).isBefore(item.endDate.toString()) && moment(currentDateTime).isAfter(item.startDate.toString()),
                          bookingID: item.id,
                          clientName: item.name,
                          display: !moment(currentDateTime).isAfter(item.endDate.toString()),
                          productID: item.productId,
                          quantity: item.quantity,
                        }
                      })
                    dispatch(updateBookingsInfo(singleSeller.sellerID, relevantBookings))
                  })
              })
            }
          })
      })
      dispatch({
        type: constants.INITIATE_FILTERED_TABLE_ENTRIES
      })
    })
  }
}

// Updating Seller information in store
export function updateSellerInfo(info: ISellerInfo[]): any {
  return {
    payload: info,
    type: constants.UPDATE_SELLERS_INFO,
  }
}

// Updating Products information in store
export function updateProductsInfo(sellerID: string, productsInfo: IUpdateProductsInfo[]): any {
  return {
    productsInfo,
    sellerID,
    type: constants.UPDATE_PRODUCTS_INFO,
  }
}

// Updating Bookings information in store
export function updateBookingsInfo(sellerID: string, bookingsInfo: IUpdateBookingsInfo[]): any {
  return {
    bookingsInfo,
    sellerID,
    type: constants.UPDATE_BOOKINGS_INFO,
  }
}
