export interface IBookingEntry {
  active?: boolean;
  display: boolean;
  bookingID?: string;
  productName?: string,
  quantity: number,
  rate: number,
  clientName: string
}

export interface ISingleEntry {
  sellerName: string;
  sellerID: string;
  bookings: IBookingEntry[];
}