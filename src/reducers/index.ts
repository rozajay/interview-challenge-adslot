import { ApplicationAction } from '../actions'
import {
  UPDATE_FILTERED_TABLE_ENTRIES,
  UPDATE_PRODUCTS_INFO,
  UPDATE_SELLERS_INFO,
  UPDATE_BOOKINGS_INFO,
  INITIATE_FILTERED_TABLE_ENTRIES
} from '../constants/DefaultConstants'
import { ApplicationState } from '../state/ApplicationState'
import { ISingleEntry } from '../types/index'

export function reducer(state: ApplicationState, action: ApplicationAction): ApplicationState {

  switch (action.type) {
    case INITIATE_FILTERED_TABLE_ENTRIES:
      return { ...state, filteredTableEntries: state.tableEntries }
    case UPDATE_FILTERED_TABLE_ENTRIES:
      let currentList: ISingleEntry[];
      let newList: ISingleEntry[]
      if (action.text !== "") {
        currentList = state.filteredTableEntries
        const filterText = action.text.toLowerCase()
        newList = currentList.map((item: any) => {
          const newBookings = item.bookings.filter((booking: any) => {
            const productName = booking.productName.toLowerCase()
            return productName.includes(filterText)
          })
          return {
            bookings: newBookings,
            sellerID: item.sellerID,
            sellerName: item.sellerName,
          }
        })
      } else {
        newList = state.tableEntries
      }
      return { ...state, filteredTableEntries: newList }
    case UPDATE_BOOKINGS_INFO:
      let newTableEntries = state.tableEntries
      newTableEntries = newTableEntries.map((singleEntry: any) => {
        if (action.sellerID === singleEntry.sellerID && singleEntry.bookings.length > 0) {
          action.bookingsInfo.map((item: any) => {
            const productInfo = singleEntry.bookings.filter((booking: any) => booking.clientName === '')
            singleEntry.bookings.push({
              active: item.active,
              bookingID: item.bookingID.substring(0, 5).toUpperCase(),
              clientName: item.clientName,
              display: item.display,
              productID: item.productID,
              productName: productInfo.filter((product: any) => product.productID === item.productID)[0].productName,
              quantity: item.quantity,
              rate: productInfo.filter((product: any) => product.productID === item.productID)[0].rate
            })
          })
        } else {
          return singleEntry
        }
      })
      return { ...state };
    case UPDATE_PRODUCTS_INFO:
      let revisedTableEntries = state.tableEntries;
      revisedTableEntries = revisedTableEntries.map((singleEntry: any) => {
        if (action.sellerID === singleEntry.sellerID) {
          if (action.productsInfo.length > 0) {
            action.productsInfo.map((item: any) => {
              singleEntry.bookings.push(item)
            })
          }
          return singleEntry
        } else {
          return singleEntry
        }
      })
      return { ...state, tableEntries: revisedTableEntries };
    case UPDATE_SELLERS_INFO:
      const tableEntries = action.payload.map((name) => {
        return {
          bookings: [],
          sellerID: name.sellerID,
          sellerName: name.sellerName,
        }
      })
      return { ...state, tableEntries };
  }
  return state;
}
