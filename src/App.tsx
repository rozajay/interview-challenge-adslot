import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux'
import * as actions from './actions/index';
import './App.css';
import Header from './components/Header/Header.ui'
import { Table } from './components/Table/Table.ui'
import { ApplicationState } from './state/ApplicationState'
import { ISingleEntry } from './types/index'

interface IAppProps {
  tableEntries: ISingleEntry[];
  filteredTableEntries: ISingleEntry[];
  fetchTableInfo: () => void;
}

class App extends React.Component<IAppProps> {
  componentDidMount() {
    this.props.fetchTableInfo();
  }
  public render() {
    return (
      <div className="App">
        <Header />
        {this.props.filteredTableEntries.map((singleEntry, index) => {
          return (<Table key={index} tableData={singleEntry} />)
        })}
      </div>
    );
  }
}

function mapStateToProps({ tableEntries, filteredTableEntries }: ApplicationState) {
  return {
    filteredTableEntries,
    tableEntries
  }
}

function mapDispatchToProps(dispatch: Dispatch<actions.ApplicationAction>) {
  return {
    fetchTableInfo: () => dispatch(actions.fetchTableInfo())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
