import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import App from './App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { ApplicationState } from './state/ApplicationState';
import { InitialState } from './state/InitialState';

import { reducer } from './reducers/index'
ReactDOM.render(
  <Provider store={createStore<ApplicationState, any, any, any>(reducer, InitialState, applyMiddleware(thunkMiddleware))}>
    <App />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
