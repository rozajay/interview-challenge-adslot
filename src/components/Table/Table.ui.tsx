import * as React from 'react'
import './styles.css'
import { ISingleEntry } from '../../types/index'

interface ITableProps {
  tableData: ISingleEntry
}

export class Table extends React.Component<ITableProps> {
  render() {
    const renderTableHeader =
      <thead>
        <tr className="row-color-inactive">
          <th scope="col" >ID</th>
          <th scope="col" >Product Name</th>
          <th scope="col" className="align-right">Quantity</th>
          <th scope="col" className="align-right">Rate</th>
          <th scope="col" className="align-right">Cost</th>
        </tr>
      </thead>

    const rendernoActiveBookings =
      <tbody>
        <tr className="row-color-inactive">
          <td>No active bookings.</td>
        </tr>
      </tbody>

    return (
      <div className="table">
        <h2 className="table-title"> {this.props.tableData.sellerName} </h2>
        <table className="table table-bordered">
          {this.props.tableData.bookings.length > 0 ? renderTableHeader : null}
          {this.props.tableData.bookings.length > 0 ?
            <tbody>
              {this.props.tableData.bookings.filter(item => item.clientName !== '' && item.display !== false).map((singleEntry, i) => {
                const output =
                  <tr key={i} className={singleEntry.active ? "row-color-active" : "row-color-inactive"}>
                    <th scope="row">{singleEntry.bookingID}</th>
                    <td>{singleEntry.productName}</td>
                    <td className="align-right">{singleEntry.quantity}</td>
                    <td className="align-right">{`$` + singleEntry.rate}</td>
                    <td className="align-right">{`$` + (singleEntry.quantity / 1000) * singleEntry.rate}</td>
                  </tr>
                return (output)

              })}
            </tbody>
            : rendernoActiveBookings}
        </table>
      </div>
    )
  }
}