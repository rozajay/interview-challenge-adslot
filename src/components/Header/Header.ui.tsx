import * as React from 'react'
import { connect } from 'react-redux';
import { Dispatch } from 'redux'
import logo from '../../assets/adslot-logo.png'
import './styles.css'
import * as actions from '../../actions/index';
import { ApplicationState } from '../../state/ApplicationState'

interface IHeaderProps {
  handleChange: (text: string) => void;
}

class Header extends React.Component<IHeaderProps> {
  render() {
    return (
      <div className="header">
        <img src={logo} alt="logo" />
        <h1 className="header-title">
          Bookings
        <input className="header-search" placeholder="  Search for booking by product name" onChange={(e) => this.props.handleChange(e.currentTarget.value)} />
        </h1>
        <hr className="header-line"></hr>
      </div>
    )
  }
}

function mapStateToProps({ filteredTableEntries, tableEntries }: ApplicationState) {
  return {
    filteredTableEntries,
    tableEntries
  }
}

function mapDispatchToProps(dispatch: Dispatch<actions.ApplicationAction>) {
  return {
    handleChange: (text: string) => dispatch(actions.updateFilteredTable(text))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
